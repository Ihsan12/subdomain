var subdomain = require('express-subdomain');
var express = require('express');
var app = express();
var router = express.Router();

// //api specific routes
// router.get('/', function(req, res) {
//     res.send('Welcome to our API!');
// });
//
// router.get('/users', function(req, res) {
//     res.json([
//         { name: "Brian" }
//     ]);
// });
//
// // app.use(router);
// app.use(subdomain('api', router));

//api specific routes
router.get('/', function(req, res) {
    res.send('Welcome to our API!');
});

router.get('/users', function(req, res) {
    res.json([
        { name: "Brian" }
    ]);
});

app.use(subdomain('api', router));

// example.com
// app.get('/', function(req, res) {
//     res.send('Homepage');
// });

app.listen(3000, function() {
    console.log('NodeJS Server Started at ' + 'localhost' + ':' + 3000)
});
